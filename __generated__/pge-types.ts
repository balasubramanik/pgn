export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: any;
};

export type AccountAlert = {
   __typename?: 'AccountAlert';
  description?: Maybe<Scalars['String']>;
  enabled?: Maybe<Scalars['Boolean']>;
};

export type AccountCustomer = {
   __typename?: 'AccountCustomer';
  language: Scalars['String'];
  email: Scalars['String'];
  uid: Scalars['String'];
  personName: Scalars['String'];
  timestamp: Scalars['DateTime'];
  encryptedPersonId: Scalars['String'];
  totalAccounts: Scalars['Int'];
  groups?: Maybe<Array<Maybe<Group>>>;
};


export type AccountCustomerGroupsArgs = {
  groupInfoParams?: Maybe<GroupInfoParams>;
};

export type AccountDetail = {
   __typename?: 'AccountDetail';
  accountNumber: Scalars['String'];
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId?: Maybe<Scalars['String']>;
  isDefault: Scalars['Boolean'];
  isLoggedInUserOnAccount: Scalars['Boolean'];
  description?: Maybe<Scalars['String']>;
  relationType: Scalars['String'];
  accountType: AccountType;
  mainCustomerName: Scalars['String'];
  coCustomerNames?: Maybe<Array<Maybe<Scalars['String']>>>;
  serviceAddresses?: Maybe<Array<Maybe<ServiceAddress>>>;
  mailingAddress?: Maybe<ServiceAddress>;
  paymentEligibility?: Maybe<OnetimePaymentEligibilityResponse>;
  autopaySameDayEnrolled?: Maybe<AutoPaymentStartResponse>;
  preferredDueDateInfo?: Maybe<PreferredDueDateInfoResponse>;
  isPaperlessBillEnrolled?: Maybe<IsPaperlessBillEnrolledResponse>;
  equalpay?: Maybe<PaymentPlanTypeResponse>;
  billInfo?: Maybe<BillInfo>;
  peaktimeRebate?: Maybe<PeakTimeRebateProgramStatus>;
  renewableEnrollment?: Maybe<RenewableEnrollment>;
  pendingDisconnect?: Maybe<PendingDisconnectStatus>;
  isActive: Scalars['Boolean'];
  alertDetails?: Maybe<AlertDetails>;
};

export type AccountDetailList = {
   __typename?: 'AccountDetailList';
  totalCount: Scalars['Int'];
  timestamp: Scalars['DateTime'];
  accounts?: Maybe<Array<Maybe<AccountDetail>>>;
};

export type AccountDetailListParams = {
  filter?: Maybe<FilterParams>;
  paging?: Maybe<PagingParams>;
  sort?: Maybe<SortParams>;
  groupId?: Maybe<Scalars['ID']>;
};

export type AccountDetailParams = {
  accountNumberList: Array<AccountParams>;
  filter?: Maybe<FilterParams>;
  paging?: Maybe<PagingParams>;
  sort?: Maybe<SortParams>;
};

export type AccountDetailsRequest = {
  description?: Maybe<Scalars['String']>;
  isDefault?: Maybe<Scalars['Boolean']>;
};

export type AccountDetailsResponse = {
  accountNumber?: Maybe<Scalars['String']>;
  relationType?: Maybe<Scalars['String']>;
  accountType?: Maybe<Scalars['String']>;
  mainCustomerName?: Maybe<Scalars['String']>;
  coCustomerNames?: Maybe<Array<Maybe<Scalars['String']>>>;
  serviceAddresses?: Maybe<Array<Maybe<ServiceAddressResponse>>>;
  mailingAddress?: Maybe<ServiceAddressResponse>;
};

export type AccountGroup = {
   __typename?: 'AccountGroup';
  groupId: Scalars['String'];
  groupCode?: Maybe<Scalars['String']>;
  groupName?: Maybe<Scalars['String']>;
  isDefault?: Maybe<Scalars['Boolean']>;
  isActive?: Maybe<Scalars['Boolean']>;
  accountNumbers?: Maybe<Array<Maybe<Scalars['String']>>>;
  numberOfAccounts?: Maybe<Scalars['Int']>;
  groupType?: Maybe<GroupType>;
};

export type AccountGroupParams = {
  encryptedaccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type AccountGroupResponse = ResponseBase & {
   __typename?: 'AccountGroupResponse';
  group?: Maybe<Group>;
  success?: Maybe<Scalars['Boolean']>;
  code?: Maybe<Scalars['Int']>;
  message?: Maybe<Scalars['String']>;
};

export enum AccountGroupVerificationType {
  DateOfBirth = 'DateOfBirth',
  EmployeeIdentificationNumber = 'EmployeeIdentificationNumber',
  PrimaryNotificationPhone = 'PrimaryNotificationPhone'
}

export type AccountInfoParams = {
  filter?: Maybe<FilterParams>;
  paging?: Maybe<PagingParams>;
  sort?: Maybe<SortParams>;
  groupId?: Maybe<Scalars['String']>;
};

export enum AccountLookUpType {
  Account = 'ACCOUNT',
  Phone = 'PHONE'
}

export type AccountParams = {
  accountNumber: Scalars['String'];
  encryptedPersonId?: Maybe<Scalars['String']>;
};

export type AccountPerson = {
   __typename?: 'AccountPerson';
  encryptedPersonId?: Maybe<Scalars['String']>;
  fullName?: Maybe<Scalars['String']>;
  personRelationshipType?: Maybe<RelationshipType>;
};

export type AccountPersonInput = {
  encryptedPersonId?: Maybe<Scalars['String']>;
  fullName?: Maybe<Scalars['String']>;
  personRelationshipType?: Maybe<RelationshipType>;
  changeReason?: Maybe<Scalars['String']>;
  employerName?: Maybe<Scalars['String']>;
};

export type AccountRemoveAccountsResponse = ResponseBase & {
   __typename?: 'AccountRemoveAccountsResponse';
  success?: Maybe<Scalars['Boolean']>;
  code?: Maybe<Scalars['Int']>;
  message?: Maybe<Scalars['String']>;
};

export type AccountResponse = {
  personName?: Maybe<Scalars['String']>;
  personId?: Maybe<Scalars['String']>;
  userName?: Maybe<Scalars['String']>;
  accounts?: Maybe<Array<Maybe<AccountDetailsResponse>>>;
};

export enum AccountSort {
  Default = 'DEFAULT',
  Accountnumber = 'ACCOUNTNUMBER',
  Duedate = 'DUEDATE'
}

export enum AccountType {
  Res = 'RES',
  Com = 'COM'
}

export type AddAccountGroupInput = {
  groupName?: Maybe<Scalars['String']>;
  groupId?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  createdDate?: Maybe<Scalars['String']>;
  numberOfAccounts?: Maybe<Scalars['Int']>;
  groupType?: Maybe<GroupType>;
  isPrimary?: Maybe<Scalars['Boolean']>;
};

export type AddGroupRequest = {
  userName?: Maybe<Scalars['String']>;
  groupCode?: Maybe<Scalars['String']>;
  groupName?: Maybe<Scalars['String']>;
  groupType?: Maybe<GroupTypeRequest>;
  isDefault?: Maybe<Scalars['Boolean']>;
};

export type AddGroupResponse = {
  group_id?: Maybe<Scalars['String']>;
};

export type AdditionalInfo = {
   __typename?: 'AdditionalInfo';
  dateOfBirth?: Maybe<Scalars['String']>;
  primaryPhone?: Maybe<Scalars['String']>;
  emailAddress?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  socialSecurityNumber?: Maybe<Scalars['String']>;
  alternatePhoneInfo?: Maybe<AlternatePhone>;
  mailingAddress?: Maybe<Address>;
  previousAddress?: Maybe<Address>;
  federalInformation?: Maybe<FederalInfo>;
  stateInformation?: Maybe<StateInfo>;
  employmentInformation?: Maybe<EmploymentInfo>;
  hasSocialSecurityInfo?: Maybe<Scalars['Boolean']>;
  hasStateInfo?: Maybe<Scalars['Boolean']>;
  hasFederalInfo?: Maybe<Scalars['Boolean']>;
  mailingAndServiceAddressesSame?: Maybe<Scalars['Boolean']>;
  registerForOnlineAccess?: Maybe<Scalars['Boolean']>;
  hasExistingPrimaryIdentification?: Maybe<Scalars['Boolean']>;
};

export type AdditionalInfoInput = {
  dateOfBirth?: Maybe<Scalars['String']>;
  primaryPhone?: Maybe<Scalars['String']>;
  emailAddress?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  socialSecurityNumber?: Maybe<Scalars['String']>;
  alternatePhoneInfo?: Maybe<AlternatePhoneInput>;
  mailingAddress?: Maybe<AddressInput>;
  previousAddress?: Maybe<AddressInput>;
  federalInformation?: Maybe<FederalInfoInput>;
  stateInformation?: Maybe<StateInfoInput>;
  employmentInformation?: Maybe<EmploymentInfoInput>;
  hasSocialSecurityInfo?: Maybe<Scalars['Boolean']>;
  hasStateInfo?: Maybe<Scalars['Boolean']>;
  hasFederalInfo?: Maybe<Scalars['Boolean']>;
  mailingAndServiceAddressesSame?: Maybe<Scalars['Boolean']>;
  registerForOnlineAccess?: Maybe<Scalars['Boolean']>;
  hasExistingPrimaryIdentification?: Maybe<Scalars['Boolean']>;
};

export type AddRemoveAccountInput = {
  accountNumber: Scalars['String'];
  encryptedBusinessPersonId?: Maybe<Scalars['String']>;
  action: AddRemoveOperation;
};

export type AddRemoveAccountRequest = {
  businessPersonId?: Maybe<Scalars['String']>;
  accountNumber?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  isDefault?: Maybe<Scalars['Boolean']>;
  isActive?: Maybe<Scalars['Boolean']>;
  action?: Maybe<AddRemoveOperation>;
};

export type AddRemoveAccountsInput = {
  groupId: Scalars['String'];
  accounts: Array<Maybe<AddRemoveAccountInput>>;
};

export type AddRemoveAccountsRequest = {
  accounts?: Maybe<Array<Maybe<AddRemoveAccountRequest>>>;
};

export type AddRemoveAccountsResponse = {
  result?: Maybe<Scalars['Boolean']>;
};

export enum AddRemoveOperation {
  Add = 'ADD',
  Remove = 'REMOVE'
}

export type Address = {
   __typename?: 'Address';
  addressLine1?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
};

export type AddressInput = {
  addressLine1?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  qasVerified?: Maybe<Scalars['Boolean']>;
};

export type Alert = {
   __typename?: 'Alert';
  description?: Maybe<Scalars['String']>;
  type?: Maybe<NotificationType>;
  sequence?: Maybe<Scalars['Float']>;
  originalValue?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
  isEmail?: Maybe<Scalars['Boolean']>;
  encryptedEmailPrefId?: Maybe<Scalars['String']>;
  encryptedEmailContactId?: Maybe<Scalars['String']>;
  isText?: Maybe<Scalars['Boolean']>;
  encryptedTextPrefId?: Maybe<Scalars['String']>;
  encryptedTextContactId?: Maybe<Scalars['String']>;
};

export type AlertDetails = {
   __typename?: 'AlertDetails';
  phoneNumber?: Maybe<Scalars['String']>;
  phoneSequence?: Maybe<Scalars['Float']>;
  notEnrolled?: Maybe<Scalars['Boolean']>;
  alerts?: Maybe<Array<Maybe<Alert>>>;
};

export type Alertinput = {
  description?: Maybe<Scalars['String']>;
  type?: Maybe<NotificationType>;
  sequence?: Maybe<Scalars['Float']>;
  originalValue?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
  isEmail?: Maybe<Scalars['Boolean']>;
  encryptedEmailPrefId?: Maybe<Scalars['String']>;
  encryptedEmailContactId?: Maybe<Scalars['String']>;
  isText?: Maybe<Scalars['Boolean']>;
  encryptedTextPrefId?: Maybe<Scalars['String']>;
  encryptedTextContactId?: Maybe<Scalars['String']>;
};

export type AlternatePhone = {
   __typename?: 'AlternatePhone';
  alternatePhoneNumber?: Maybe<Scalars['String']>;
  alternatePhoneType?: Maybe<PhoneType>;
  alternatePhoneText?: Maybe<Scalars['String']>;
};

export type AlternatePhoneInfoInput = {
  alternatePhoneExt?: Maybe<Scalars['String']>;
  alternatePhoneNumber?: Maybe<Scalars['String']>;
  alternatePhoneType?: Maybe<PhoneType>;
};

export type AlternatePhoneInput = {
  alternatePhoneNumber?: Maybe<Scalars['String']>;
  alternatePhoneType?: Maybe<PhoneType>;
  alternatePhoneText?: Maybe<Scalars['String']>;
};

export type AutoPayment = {
   __typename?: 'AutoPayment';
  autoPayId?: Maybe<Scalars['String']>;
  autoPaySource?: Maybe<Scalars['String']>;
  paymentType?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  accountNumber?: Maybe<Scalars['String']>;
  serviceAddressLineOne?: Maybe<Scalars['String']>;
  serviceAddressLineTwo?: Maybe<Scalars['String']>;
  amountDue?: Maybe<Scalars['Float']>;
  customerName?: Maybe<Scalars['String']>;
  saveBankinfo?: Maybe<Scalars['Boolean']>;
  guestEmailAddress?: Maybe<Scalars['String']>;
  guestTextPhone?: Maybe<Scalars['String']>;
  useSavedBankInfo?: Maybe<Scalars['Boolean']>;
  savedBankInfo?: Maybe<PaymentSavedBankInfo>;
  newBankInfo?: Maybe<PaymentNewBankInfo>;
};

export type AutoPaymentCancelRequest = {
  isPaymentDueInNextThreeDays?: Maybe<Scalars['Boolean']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type AutoPaymentCancelResponse = {
   __typename?: 'AutoPaymentCancelResponse';
  success?: Maybe<Scalars['Boolean']>;
};

export type AutoPaymentDownloadPdfRequest = {
  name?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  serviceAddress?: Maybe<Scalars['String']>;
  bankAccountRoutingNumber?: Maybe<Scalars['String']>;
  bankAccountNumber?: Maybe<Scalars['String']>;
  confirmationNumber?: Maybe<Scalars['String']>;
};

export type AutoPaymentInfoResponse = {
   __typename?: 'AutoPaymentInfoResponse';
  eligibility?: Maybe<OnetimePaymentEligibilityResponse>;
  preferredDueDateEligibility?: Maybe<PreferredDueDateEligibilityResponse>;
  sameDayEnrolled?: Maybe<AutoPaymentStartResponse>;
  preferredDueDateInfo?: Maybe<PreferredDueDateInfoResponse>;
  paymentBankInfo?: Maybe<PaymentBankInfoResponse>;
};

export type AutoPaymentPdfResponse = {
   __typename?: 'AutoPaymentPdfResponse';
  success?: Maybe<Scalars['Boolean']>;
  pdf?: Maybe<Scalars['String']>;
};

export type AutoPaymentRequest = {
  encryptedAccountNumber: Scalars['String'];
  customerName?: Maybe<Scalars['String']>;
  serviceAddressLineOne?: Maybe<Scalars['String']>;
  serviceAddressLineTwo?: Maybe<Scalars['String']>;
  amountDue?: Maybe<Scalars['Float']>;
  useSavedBankInfo?: Maybe<Scalars['Boolean']>;
  savedBankInfo?: Maybe<PaymentSavedBankInfoRequest>;
  newBankInfo?: Maybe<PaymentNewBankInfoRequest>;
};

export type AutoPaymentResponse = {
   __typename?: 'AutoPaymentResponse';
  confirmationNumber?: Maybe<Scalars['String']>;
};

export enum AutoPaymentSource {
  Unknown = 'UNKNOWN',
  Pgeapay = 'PGEAPAY',
  Bankcardapay = 'BANKCARDAPAY'
}

export type AutoPaymentStartResponse = {
   __typename?: 'AutoPaymentStartResponse';
  startDate?: Maybe<Scalars['String']>;
  autoPaySource?: Maybe<AutoPaymentSource>;
  billMatrixUrl?: Maybe<Scalars['String']>;
};

export type BillInfo = {
   __typename?: 'BillInfo';
  amountDue?: Maybe<Scalars['Float']>;
  balanceDueDate?: Maybe<Scalars['String']>;
  dueDate?: Maybe<Scalars['String']>;
  lastPaymentAmount?: Maybe<Scalars['Float']>;
  lastPaymentDate?: Maybe<Scalars['String']>;
  isAccountPayable?: Maybe<Scalars['Boolean']>;
  isNewAccount?: Maybe<Scalars['Boolean']>;
  futurePaymentDate?: Maybe<Scalars['String']>;
  oneTimeFuturePaymentScheduled?: Maybe<Scalars['Boolean']>;
  multipleFuturePaymentsScheduled?: Maybe<Scalars['Boolean']>;
  enrolledInTPA?: Maybe<Scalars['Boolean']>;
};

export type BillInfoParams = {
  accountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export enum BillingAndPaymentDelimiterType {
  Unknown = 'Unknown',
  Csv = 'Csv',
  Pipe = 'Pipe'
}

export type BillingAndPaymentHistoryDetailsDownloadResponse = {
   __typename?: 'BillingAndPaymentHistoryDetailsDownloadResponse';
  byteString?: Maybe<Scalars['String']>;
};

export type BillingAndPaymentHistoryDetailsInput = {
  isCustomGroup?: Maybe<Scalars['Boolean']>;
  customGroupName?: Maybe<Scalars['String']>;
  encryptedPersonId?: Maybe<Scalars['String']>;
  encryptedAccountNumbers?: Maybe<Array<Maybe<Scalars['String']>>>;
  startDate?: Maybe<Scalars['DateTime']>;
  endDate?: Maybe<Scalars['DateTime']>;
  sortedByDueDate?: Maybe<Scalars['Boolean']>;
  reportType?: Maybe<BillingAndPaymentReportType>;
  delimeterType?: Maybe<BillingAndPaymentDelimiterType>;
};

export type BillingAndPaymentHistoryDetailsResponse = {
   __typename?: 'BillingAndPaymentHistoryDetailsResponse';
  encryptedAccountNumbers?: Maybe<Array<Maybe<Scalars['String']>>>;
  billingSummaries?: Maybe<Array<Maybe<BillingAndPaymentHistorySummary>>>;
  totalAmount?: Maybe<Scalars['Float']>;
  totalAccounts?: Maybe<Scalars['Float']>;
};

export type BillingAndPaymentHistorySummary = {
   __typename?: 'BillingAndPaymentHistorySummary';
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  encryptedBillingId?: Maybe<Scalars['String']>;
  totalKwh?: Maybe<Scalars['Float']>;
  amountDue?: Maybe<Scalars['Float']>;
  dueDate?: Maybe<Scalars['DateTime']>;
  billDate?: Maybe<Scalars['DateTime']>;
  details?: Maybe<Array<Maybe<BillingAndPaymentSummaryDetail>>>;
};

export enum BillingAndPaymentReportType {
  Unknown = 'Unknown',
  LongForm = 'LongForm',
  ShortForm = 'ShortForm',
  Accounting = 'Accounting'
}

export type BillingAndPaymentSummaryDetail = {
   __typename?: 'BillingAndPaymentSummaryDetail';
  amount?: Maybe<Scalars['Float']>;
  kwh?: Maybe<Scalars['Float']>;
  serviceAddress?: Maybe<Scalars['String']>;
};

export type BillingPeriod = {
   __typename?: 'BillingPeriod';
  averageTemperature?: Maybe<Scalars['Int']>;
  date?: Maybe<Scalars['String']>;
  totalCost?: Maybe<Scalars['Int']>;
  totalKwh?: Maybe<Scalars['Int']>;
};

export type BusinessPersonRequest = {
  businessPersonId?: Maybe<Scalars['String']>;
  accountNumberList?: Maybe<Array<Maybe<Scalars['String']>>>;
};

export type ChangeEmailRequest = {
  newEmail?: Maybe<Scalars['String']>;
};

export type ChangeEmailResponse = {
   __typename?: 'ChangeEmailResponse';
  success?: Maybe<Scalars['Boolean']>;
};

export type ChangePasswordInfo = {
   __typename?: 'ChangePasswordInfo';
  hasPerson?: Maybe<Scalars['Boolean']>;
  sameAsOldPassword?: Maybe<Scalars['Boolean']>;
};

export type ChangePasswordInfoRequest = {
  password?: Maybe<Scalars['String']>;
};

export type ChangePasswordInfoResponse = {
   __typename?: 'ChangePasswordInfoResponse';
  hasPerson?: Maybe<HasPersonIdResponse>;
  sameAsOldPassword?: Maybe<SameAsOldPasswordResponse>;
};

export type ChangePasswordUpdateRequest = {
  password?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type ChangePasswordUpdateResponse = {
   __typename?: 'ChangePasswordUpdateResponse';
  success?: Maybe<Scalars['Boolean']>;
};

export type ChangePinUpdateRequest = {
  newPinCode?: Maybe<Scalars['String']>;
};

export type ChangePinUpdateResponse = {
   __typename?: 'ChangePinUpdateResponse';
  success?: Maybe<Scalars['Boolean']>;
};

export type CoCustomerInfo = {
   __typename?: 'CoCustomerInfo';
  encryptedPersonId?: Maybe<Scalars['String']>;
  personalIdentificationType?: Maybe<PersonalIdentificationType>;
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  middleName?: Maybe<Scalars['String']>;
};

export enum CocustomerRelationshipType {
  None = 'None',
  Spouse = 'Spouse',
  DomesticPartner = 'DomesticPartner',
  Relative = 'Relative',
  Roommate = 'Roommate'
}

export type CommercialAccountValidation = {
   __typename?: 'CommercialAccountValidation';
  isValidBusinessAccount: Scalars['Boolean'];
  shouldUseBookkeeperRegistration: Scalars['Boolean'];
};

export type CommercialContactInfoInput = {
  bookKeeperName?: Maybe<Scalars['String']>;
  bookKeeperPhone?: Maybe<Scalars['String']>;
  bookKeeperPhoneExt?: Maybe<Scalars['String']>;
  contactName?: Maybe<Scalars['String']>;
  primaryPhone?: Maybe<Scalars['String']>;
  primaryPhoneExt?: Maybe<Scalars['String']>;
};

export type CommercialRegistrationRequest = {
  AccountNumber?: Maybe<Scalars['String']>;
  EmailAddress?: Maybe<Scalars['String']>;
  Password?: Maybe<Scalars['String']>;
  FirstName?: Maybe<Scalars['String']>;
  MiddleName?: Maybe<Scalars['String']>;
  LastName?: Maybe<Scalars['String']>;
  VerificationType?: Maybe<VerificationType>;
  VerificationValue?: Maybe<Scalars['String']>;
  IsCoApplicantOfCommercialAccount?: Maybe<Scalars['Boolean']>;
};

export type ContactlogPersonLeavingBehindRequest = {
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type Coordinates = {
   __typename?: 'Coordinates';
  lng: Scalars['Float'];
  lat: Scalars['Float'];
  other?: Maybe<Scalars['Float']>;
};

export type CreateAccountGroupInput = {
  accountNumber: Scalars['String'];
  businessName: Scalars['String'];
  verificationValue: Scalars['String'];
  verificationType?: Maybe<AccountGroupVerificationType>;
};

export type CreateGroupAccountsInput = {
  accountNumber: Scalars['String'];
  encryptedBusinessPersonId?: Maybe<Scalars['String']>;
  action: AddRemoveOperation;
};

export type CreateGroupInput = {
  groupCode: Scalars['String'];
  groupName: Scalars['String'];
  accounts: Array<CreateGroupAccountsInput>;
};

export type CurrentBillDetailsResponse = {
  accountNumber?: Maybe<Scalars['String']>;
  amountDue?: Maybe<Scalars['Float']>;
  dueDate?: Maybe<Scalars['String']>;
  isAccountPayable?: Maybe<Scalars['Boolean']>;
  isNewAccount?: Maybe<Scalars['Boolean']>;
  lastPaymentDate?: Maybe<Scalars['String']>;
  futurePaymentDate?: Maybe<Scalars['String']>;
  lastPaymentAmount?: Maybe<Scalars['Float']>;
  oneTimeFuturePaymentScheduled?: Maybe<Scalars['Boolean']>;
  multipleFuturePaymentsScheduled?: Maybe<Scalars['Boolean']>;
  enrolledInTPA?: Maybe<Scalars['Boolean']>;
};

export type CustomerInfoInput = {
  encryptedPersonId?: Maybe<Scalars['String']>;
  personalIdentificationType?: Maybe<PersonalIdentificationType>;
  additionalInformation?: Maybe<AdditionalInfoInput>;
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  middleName?: Maybe<Scalars['String']>;
  spouseOrRegisteredDomesticPartnerType?: Maybe<CocustomerRelationshipType>;
};

export type CustomerPreferencesAccountResponse = {
  businessPersonId?: Maybe<Scalars['String']>;
  accountNumber?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  isDefault?: Maybe<Scalars['Boolean']>;
};

export type CustomerPreferencesResponse = {
  language?: Maybe<Scalars['String']>;
  hasGroups?: Maybe<Scalars['Boolean']>;
  accounts?: Maybe<Array<Maybe<CustomerPreferencesAccountResponse>>>;
};

export type CustomerServiceDisconnectedParams = {
  addressLine1?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
};

export type CustomGroupInput = {
  groupName?: Maybe<Scalars['String']>;
  groupId?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  createdDate?: Maybe<Scalars['String']>;
  numberOfAccounts?: Maybe<Scalars['Int']>;
  groupType?: Maybe<GroupType>;
  isPrimary?: Maybe<Scalars['Boolean']>;
  groupCode?: Maybe<Scalars['String']>;
  nickname?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  checked?: Maybe<Scalars['Boolean']>;
};


export type DefaultAccountInfo = {
   __typename?: 'DefaultAccountInfo';
  accountNumber?: Maybe<Scalars['String']>;
  encryptedPersonId?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type DeleteGroupInput = {
  groupId: Scalars['String'];
};

export type DeleteGroupResponse = ResponseBase & {
   __typename?: 'DeleteGroupResponse';
  success?: Maybe<Scalars['Boolean']>;
  code?: Maybe<Scalars['Int']>;
  message?: Maybe<Scalars['String']>;
};

export type Description = {
   __typename?: 'Description';
  description?: Maybe<Scalars['String']>;
  link?: Maybe<Scalars['String']>;
};

export enum Direction {
  Asc = 'ASC',
  Desc = 'DESC'
}

export type EmploymentInfo = {
   __typename?: 'EmploymentInfo';
  employmentOption?: Maybe<EmploymentType>;
  employerName?: Maybe<Scalars['String']>;
};

export type EmploymentInfoInput = {
  employmentOption?: Maybe<EmploymentType>;
  employerName?: Maybe<Scalars['String']>;
};

export enum EmploymentType {
  None = 'None',
  Employed = 'Employed',
  SelfEmployed = 'SelfEmployed',
  Retired = 'Retired',
  Unemployed = 'Unemployed'
}

export type EnergyTrackerLinkInput = {
   __typename?: 'EnergyTrackerLinkInput';
  LinkType?: Maybe<EnergyTrackerLinkInputLinkType>;
  ServiceProvider?: Maybe<EnergyTrackerLinkInputServiceProvider>;
};

export enum EnergyTrackerLinkInputLinkType {
  None = 'None',
  MyUsage = 'MyUsage',
  CompareBills = 'CompareBills',
  WaysToSave = 'WaysToSave'
}

export enum EnergyTrackerLinkInputServiceProvider {
  None = 'None',
  OPower = 'OPower',
  FirstFuel = 'FirstFuel'
}

export type Enrollments = {
   __typename?: 'Enrollments';
  descriptions?: Maybe<Array<Maybe<Description>>>;
  enrolled?: Maybe<Scalars['Boolean']>;
};

export enum FederalIdentificationType {
  UsPassport = 'USPassport',
  UsStudentVisa = 'USStudentVisa',
  UsMilitaryId = 'USMilitaryID',
  UsTemporaryVisa = 'USTemporaryVisa',
  UsImmigration = 'USImmigration',
  OregonTribalId = 'OregonTribalID'
}

export type FederalInfo = {
   __typename?: 'FederalInfo';
  federalIDType?: Maybe<FederalIdentificationType>;
  federalIDNumber?: Maybe<Scalars['String']>;
};

export type FederalInfoInput = {
  federalIDType?: Maybe<FederalIdentificationType>;
  federalIDNumber?: Maybe<Scalars['String']>;
};

export type FilterParams = {
  filterBy?: Maybe<Scalars['String']>;
  operator: Operator;
};

export enum ForgotPasswordErrorType {
  None = 'None',
  NoValidWebUser = 'NoValidWebUser',
  NoMatchFound = 'NoMatchFound',
  NoPersonFound = 'NoPersonFound',
  NoPhoneFound = 'NoPhoneFound',
  SpecialHandling = 'SpecialHandling'
}

export type ForgotPasswordRequest = {
  emailAddress?: Maybe<Scalars['String']>;
  phoneNumber?: Maybe<Scalars['String']>;
  lastFourDigitSSN?: Maybe<Scalars['String']>;
  lastFourDigitStateOrDriverID?: Maybe<Scalars['String']>;
  pinCode?: Maybe<Scalars['String']>;
  birthDate?: Maybe<Scalars['String']>;
  lastFourDigitEIN?: Maybe<Scalars['String']>;
  identificationType?: Maybe<PersonalIdentificationType>;
};

export type ForgotPasswordResponse = {
   __typename?: 'ForgotPasswordResponse';
  forgotPasswordError?: Maybe<ForgotPasswordErrorType>;
  resetPasswordFlag?: Maybe<Scalars['Boolean']>;
  securityToken?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type GetAlertDetailsRequest = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  encryptedPersonId?: Maybe<Scalars['String']>;
};

export type Group = {
   __typename?: 'Group';
  groupName?: Maybe<Scalars['String']>;
  groupId: Scalars['ID'];
  groupCode?: Maybe<Scalars['String']>;
  numberOfAccounts?: Maybe<Scalars['Int']>;
  isDefault?: Maybe<Scalars['Boolean']>;
  type?: Maybe<GroupType>;
  isDefaultAccountExists: Scalars['Boolean'];
  defaultAccount?: Maybe<DefaultAccountInfo>;
  accounts?: Maybe<Array<Maybe<AccountDetail>>>;
};


export type GroupAccountsArgs = {
  accountInfoParams?: Maybe<AccountInfoParams>;
};

export type GroupDetailsRequest = {
  groupCode?: Maybe<Scalars['String']>;
  groupName?: Maybe<Scalars['String']>;
};

export type GroupInfoParams = {
  filter?: Maybe<FilterParams>;
  paging?: Maybe<PagingParams>;
  sort?: Maybe<SortParams>;
};

export type GroupResponse = {
  groupid?: Maybe<Scalars['String']>;
  groupcode?: Maybe<Scalars['String']>;
  groupname?: Maybe<Scalars['String']>;
  isDefault?: Maybe<Scalars['Boolean']>;
  groupType?: Maybe<GroupTypeRequest>;
  accounts?: Maybe<Array<Maybe<CustomerPreferencesAccountResponse>>>;
};

export type GroupsResponse = {
  groups?: Maybe<Array<Maybe<GroupResponse>>>;
};

export enum GroupType {
  Automatic = 'Automatic',
  Custom = 'Custom',
  Virtual = 'Virtual'
}

export enum GroupTypeRequest {
  Custom = 'CUSTOM',
  Auto = 'AUTO'
}

export type GuestPayInfoResponse = {
   __typename?: 'GuestPayInfoResponse';
  accountNumber?: Maybe<Scalars['String']>;
  amountDue?: Maybe<Scalars['Float']>;
  dueDate?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  guestEmailAddress?: Maybe<Scalars['String']>;
  guestTextPhone?: Maybe<Scalars['String']>;
  newBankInfo?: Maybe<PaymentNewBankInfo>;
  saveBankinfo?: Maybe<Scalars['Boolean']>;
  savedBankInfo?: Maybe<PaymentSavedBankInfo>;
  serviceAddressLineOne?: Maybe<Scalars['String']>;
  serviceAddressLineTwo?: Maybe<Scalars['String']>;
  useSavedBankInfo?: Maybe<Scalars['Boolean']>;
};

export type GuestPaymentAccountRequest = {
  zipCode: Scalars['String'];
  phoneNumber?: Maybe<Scalars['String']>;
  accountNumber?: Maybe<Scalars['String']>;
  identificationType: IdentificationType;
};

export type GuestPaymentConfirmationResponse = {
   __typename?: 'GuestPaymentConfirmationResponse';
  success?: Maybe<Scalars['Boolean']>;
  confirmationNumber?: Maybe<Scalars['String']>;
};

export type GuestPaymentDetailsResponse = {
   __typename?: 'GuestPaymentDetailsResponse';
  eligibility?: Maybe<GuestPaymentEligibilityResponse>;
  paymentInfo?: Maybe<GuestPayInfoResponse>;
};

export type GuestPaymentDownloadPdfRequest = {
  confirmationNumber?: Maybe<Scalars['String']>;
  houseNumber?: Maybe<Scalars['String']>;
  bankAccountNumber?: Maybe<Scalars['String']>;
  paymentDate?: Maybe<Scalars['String']>;
  paymentAmount?: Maybe<Scalars['String']>;
};

export type GuestPaymentEligibilityResponse = {
   __typename?: 'GuestPaymentEligibilityResponse';
  success?: Maybe<Scalars['Boolean']>;
  serviceAddress?: Maybe<Scalars['String']>;
};

export type GuestPaymentPdfResponse = {
   __typename?: 'GuestPaymentPdfResponse';
  success?: Maybe<Scalars['Boolean']>;
  pdf?: Maybe<Scalars['String']>;
};

export type GuestPaymentSubmitRequest = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  paymentAmount?: Maybe<Scalars['String']>;
  serviceAddressLineOne?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  saveBankInfo?: Maybe<Scalars['Boolean']>;
  guestEmailAddress?: Maybe<Scalars['String']>;
  guestTextPhone?: Maybe<Scalars['String']>;
  newBankInfo?: Maybe<PaymentNewBankInfoRequest>;
};

export type GuestPaymentVerifyResponse = {
   __typename?: 'GuestPaymentVerifyResponse';
  success?: Maybe<Scalars['Boolean']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type HasPersonIdResponse = {
   __typename?: 'HasPersonIdResponse';
  hasPerson?: Maybe<Scalars['Boolean']>;
};

export enum IdentificationType {
  PhoneNumber = 'phoneNumber',
  AccountNumber = 'accountNumber'
}

export type IsDuplicateEmailResponse = {
   __typename?: 'IsDuplicateEmailResponse';
  isDuplicate?: Maybe<Scalars['Boolean']>;
};

export type IsPaperlessBillEnrolledRequest = {
  encryptedAccountNumber: Scalars['String'];
};

export type IsPaperlessBillEnrolledResponse = {
   __typename?: 'IsPaperlessBillEnrolledResponse';
  result?: Maybe<Scalars['Boolean']>;
};

export type IsPersonalIdValidParams = {
  encryptedPersonId: Scalars['String'];
  idNumber: Scalars['String'];
};

export type MeterAccessInput = {
  hasSecurityGate?: Maybe<Scalars['Boolean']>;
  hasDog?: Maybe<Scalars['Boolean']>;
  meterAccessMessage?: Maybe<Scalars['String']>;
};

export type MonthlyUsage = {
   __typename?: 'MonthlyUsage';
  monthName?: Maybe<Scalars['String']>;
  totalKwh?: Maybe<Scalars['Int']>;
  year?: Maybe<Scalars['Int']>;
};

export type MoveServiceEligibilityRequest = {
  encryptedAccountNumber: Scalars['String'];
  serviceAddress: AddressInput;
};

export type MoveServiceEligibilityResponse = {
   __typename?: 'MoveServiceEligibilityResponse';
  eligibleForMoveService: Scalars['Boolean'];
  eligibleForGreenResource: Scalars['Boolean'];
  isCurrentlyPaperlessBilling: Scalars['Boolean'];
};

export type MoveServiceRequest = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  anyoneRemainingAtProperty?: Maybe<Scalars['Boolean']>;
  currentServiceAddress?: Maybe<AddressInput>;
  customerInformation?: Maybe<CustomerInfoInput>;
  eligibleForGreenResource?: Maybe<Scalars['Boolean']>;
  eligibleForMoveService?: Maybe<Scalars['Boolean']>;
  heatSourceType?: Maybe<Scalars['String']>;
  isCurrentlyPaperlessBilling?: Maybe<Scalars['Boolean']>;
  isElectricVehicleSelected?: Maybe<Scalars['Boolean']>;
  isPaperlessBillingSelected?: Maybe<Scalars['Boolean']>;
  isRenewablesSelected?: Maybe<Scalars['Boolean']>;
  isSmartThermostatSelected?: Maybe<Scalars['Boolean']>;
  livesAtPremise?: Maybe<Scalars['Boolean']>;
  meterAccessInfo?: Maybe<MeterAccessInput>;
  moveToServiceAddress?: Maybe<AddressInput>;
  selectedPremiseType?: Maybe<PremiseType>;
  startDate?: Maybe<Scalars['String']>;
  stopDate?: Maybe<Scalars['String']>;
  waterHeaterType?: Maybe<Scalars['String']>;
  coCustomersInformation?: Maybe<Array<Maybe<CustomerInfoInput>>>;
  preferredDueDate?: Maybe<Scalars['String']>;
  isAddressExactMatch?: Maybe<Scalars['Boolean']>;
  premiseId?: Maybe<Scalars['String']>;
};

export type MoveToServiceAddressEligibilityRequest = {
  encryptedAccountNumber: Scalars['String'];
  serviceAddress: AddressInput;
  selectedPremiseType: PremiseType;
};

export type MoveToServiceAddressEligibilityResponse = {
   __typename?: 'MoveToServiceAddressEligibilityResponse';
  isEligible: Scalars['Boolean'];
  isAddressExactMatch: Scalars['Boolean'];
  isPeakTimeRebateEligible: Scalars['Boolean'];
  premiseId: Scalars['String'];
  serviceAddressEligibilityType?: Maybe<ServiceAddressEligibilityType>;
};

export type MultiPaymentDetail = {
   __typename?: 'MultiPaymentDetail';
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  confirmationNumber?: Maybe<Scalars['String']>;
};

export type Mutation = {
   __typename?: 'Mutation';
  root?: Maybe<Scalars['String']>;
  addRemoveAccountsToGroup: AccountRemoveAccountsResponse;
  createAccountGroup: AccountGroupResponse;
  createCustomGroup: AccountGroupResponse;
  deleteGroup: DeleteGroupResponse;
  updateGroup: UpdateGroupResponse;
  updateAccountDetails?: Maybe<AccountGroupResponse>;
  updateAlerts?: Maybe<UpdateAlertsResponse>;
  updatePaperlessBill?: Maybe<UpdatePaperlessBillResponse>;
  sendActivationCode?: Maybe<SendActivationCodeResponse>;
  verifyEqualsEncryptedValue?: Maybe<VerifyEqualsEncryptedValueResponse>;
  moveServiceSubmit?: Maybe<Scalars['Boolean']>;
  reportOutage?: Maybe<ReportOutageResponse>;
  reportOutageByGuest?: Maybe<ReportOutageResponse>;
  updateAutoPayment?: Maybe<AutoPaymentResponse>;
  cancelAutoPayment?: Maybe<AutoPaymentCancelResponse>;
  submitGuestPayment?: Maybe<GuestPaymentConfirmationResponse>;
  submitOnecheckPayment?: Maybe<OnecheckPaymentSubmitResponse>;
  submitMultiplePayment?: Maybe<MutiPaymentSubmitResponse>;
  submitOnetimePayment?: Maybe<OnetimePaymentResponse>;
  addPreferredDueDate?: Maybe<PreferredDueDateAddResponse>;
  submitQuickPayment?: Maybe<QuickPaymentResponse>;
  createRenewablesEnrollmentTodo?: Maybe<Scalars['Boolean']>;
  updateRenewables?: Maybe<Scalars['Boolean']>;
  registerResidential?: Maybe<RegistrationResponse>;
  registerCommercial?: Maybe<RegistrationResponse>;
  unregister?: Maybe<UnregisterResponse>;
  startServiceCreateIneligibilityLog?: Maybe<Scalars['Boolean']>;
  startServiceSubmit?: Maybe<StartServiceResponse>;
  createPersonLeavingBehindContactLog?: Maybe<Scalars['Boolean']>;
  stopServiceSubmit?: Maybe<Scalars['String']>;
  updateAccountInfo?: Maybe<Scalars['Boolean']>;
  updateEmail?: Maybe<ChangeEmailResponse>;
  updatePassword?: Maybe<ChangePasswordUpdateResponse>;
  updatePin?: Maybe<ChangePinUpdateResponse>;
  validateAndUpdateTempPassword?: Maybe<ForgotPasswordResponse>;
};


export type MutationAddRemoveAccountsToGroupArgs = {
  payload: AddRemoveAccountsInput;
};


export type MutationCreateAccountGroupArgs = {
  payload: CreateAccountGroupInput;
};


export type MutationCreateCustomGroupArgs = {
  payload: CreateGroupInput;
};


export type MutationDeleteGroupArgs = {
  payload: DeleteGroupInput;
};


export type MutationUpdateGroupArgs = {
  payload: UpdateGroupInput;
};


export type MutationUpdateAccountDetailsArgs = {
  payload?: Maybe<CustomGroupInput>;
};


export type MutationUpdateAlertsArgs = {
  payload?: Maybe<UpdateAlertsRequest>;
};


export type MutationUpdatePaperlessBillArgs = {
  payload?: Maybe<UpdatePaperlessBillRequest>;
};


export type MutationSendActivationCodeArgs = {
  payload?: Maybe<SendActivationCodeRequest>;
};


export type MutationVerifyEqualsEncryptedValueArgs = {
  payload?: Maybe<VerifyEqualsEncryptedValueRequest>;
};


export type MutationMoveServiceSubmitArgs = {
  payload?: Maybe<MoveServiceRequest>;
};


export type MutationReportOutageArgs = {
  payload?: Maybe<ReportOutageRequest>;
};


export type MutationReportOutageByGuestArgs = {
  payload?: Maybe<ReportOutageByGuestRequest>;
};


export type MutationUpdateAutoPaymentArgs = {
  payload?: Maybe<AutoPaymentRequest>;
};


export type MutationCancelAutoPaymentArgs = {
  payload?: Maybe<AutoPaymentCancelRequest>;
};


export type MutationSubmitGuestPaymentArgs = {
  payload?: Maybe<GuestPaymentSubmitRequest>;
};


export type MutationSubmitOnecheckPaymentArgs = {
  payload?: Maybe<OnecheckPaymentSubmitRequest>;
};


export type MutationSubmitMultiplePaymentArgs = {
  payload: Array<Maybe<OnetimePaymentRequest>>;
};


export type MutationSubmitOnetimePaymentArgs = {
  payload?: Maybe<OnetimePaymentRequest>;
};


export type MutationAddPreferredDueDateArgs = {
  payload?: Maybe<PreferredDueDateAddRequest>;
};


export type MutationSubmitQuickPaymentArgs = {
  payload?: Maybe<QuickPaymentRequest>;
};


export type MutationCreateRenewablesEnrollmentTodoArgs = {
  payload?: Maybe<RenewablesEnrollmentToDoRequest>;
};


export type MutationUpdateRenewablesArgs = {
  payload?: Maybe<RenewablePowerInput>;
};


export type MutationRegisterResidentialArgs = {
  payload: ResidentialRegistrationRequest;
};


export type MutationRegisterCommercialArgs = {
  payload: CommercialRegistrationRequest;
};


export type MutationStartServiceCreateIneligibilityLogArgs = {
  payload?: Maybe<StartServiceIneligibilityLogRequest>;
};


export type MutationStartServiceSubmitArgs = {
  payload?: Maybe<StartServiceRequest>;
};


export type MutationCreatePersonLeavingBehindContactLogArgs = {
  payload?: Maybe<ContactlogPersonLeavingBehindRequest>;
};


export type MutationStopServiceSubmitArgs = {
  payload?: Maybe<StopServiceSubmitRequest>;
};


export type MutationUpdateAccountInfoArgs = {
  payload?: Maybe<SubmitUpdateInfoInput>;
};


export type MutationUpdateEmailArgs = {
  payload?: Maybe<ChangeEmailRequest>;
};


export type MutationUpdatePasswordArgs = {
  payload?: Maybe<ChangePasswordUpdateRequest>;
};


export type MutationUpdatePinArgs = {
  payload?: Maybe<ChangePinUpdateRequest>;
};


export type MutationValidateAndUpdateTempPasswordArgs = {
  payload?: Maybe<ForgotPasswordRequest>;
};

export type MutiPaymentSubmitResponse = {
   __typename?: 'MutiPaymentSubmitResponse';
  confirmationNumbers?: Maybe<Array<Maybe<MultiPaymentDetail>>>;
};

export enum NotificationType {
  Unknown = 'Unknown',
  Webpyrcv = 'WEBPYRCV',
  Webpda = 'WEBPDA',
  Webdisc = 'WEBDISC',
  Webpyrem = 'WEBPYREM',
  Webuse = 'WEBUSE',
  Webexc = 'WEBEXC',
  Paperless = 'PAPERLESS',
  Rest = 'REST'
}

export enum OnecheckDisplayOption {
  AmountDue = 'AmountDue',
  LastBilledAmount = 'LastBilledAmount'
}

export type OnecheckPaymentAccount = {
   __typename?: 'OnecheckPaymentAccount';
  accountNumber?: Maybe<Scalars['String']>;
  amountDue?: Maybe<Scalars['Float']>;
  addresses?: Maybe<Array<Maybe<Scalars['String']>>>;
  lastBilledDate?: Maybe<Scalars['String']>;
  lastBilledAmount?: Maybe<Scalars['Float']>;
  billDueDate?: Maybe<Scalars['String']>;
  downloadBillUrl?: Maybe<Scalars['String']>;
};

export type OnecheckPaymentAccountRequest = {
  accountNumber?: Maybe<Scalars['String']>;
  amountDue?: Maybe<Scalars['Float']>;
  payment?: Maybe<Scalars['Float']>;
  addresses?: Maybe<Array<Maybe<Scalars['String']>>>;
  paymentOriginal?: Maybe<Scalars['Float']>;
  lastBilledDate?: Maybe<Scalars['String']>;
  lastBilledAmount?: Maybe<Scalars['Float']>;
  billDueDate?: Maybe<Scalars['String']>;
};

export type OnecheckPaymentDownloadRemittanceFormResponse = {
   __typename?: 'OnecheckPaymentDownloadRemittanceFormResponse';
  pdf?: Maybe<Scalars['String']>;
  success?: Maybe<Scalars['Boolean']>;
};

export type OnecheckPaymentGroupInfoRequest = {
  encryptedPersonId?: Maybe<Scalars['String']>;
  customGroupName?: Maybe<Scalars['String']>;
};

export type OnecheckPaymentGroupInfoResponse = {
   __typename?: 'OnecheckPaymentGroupInfoResponse';
  numberOfAccounts?: Maybe<Scalars['Int']>;
  accounts?: Maybe<Array<Maybe<OnecheckPaymentAccount>>>;
  totalAmountDue?: Maybe<Scalars['Float']>;
};

export type OnecheckPaymentInfoResponse = {
   __typename?: 'OnecheckPaymentInfoResponse';
  amountDueDetails?: Maybe<OnecheckPaymentGroupInfoResponse>;
  lastBilledAmountDetails?: Maybe<OnecheckPaymentGroupInfoResponse>;
};

export type OnecheckPaymentSubmitRequest = {
  accountGroup?: Maybe<Scalars['String']>;
  encryptedPersonId?: Maybe<Scalars['String']>;
  displayOption?: Maybe<OnecheckDisplayOption>;
  numberOfAccounts?: Maybe<Scalars['Int']>;
  totalAmountDue?: Maybe<Scalars['Float']>;
  oneCheckTotal?: Maybe<Scalars['Float']>;
  todaysDate?: Maybe<Scalars['String']>;
  remittanceId?: Maybe<Scalars['String']>;
  isCustomGroup?: Maybe<Scalars['Boolean']>;
  accounts?: Maybe<Array<Maybe<OnecheckPaymentAccountRequest>>>;
};

export type OnecheckPaymentSubmitResponse = {
   __typename?: 'OnecheckPaymentSubmitResponse';
  success?: Maybe<Scalars['Boolean']>;
  remittanceId?: Maybe<Scalars['String']>;
};

export type OnetimePayment = {
   __typename?: 'OnetimePayment';
  paymentDate?: Maybe<Scalars['String']>;
  paymentAmount?: Maybe<Scalars['Float']>;
  personId?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  accountNumber?: Maybe<Scalars['String']>;
  serviceAddressLineOne?: Maybe<Scalars['String']>;
  serviceAddressLineTwo?: Maybe<Scalars['String']>;
  amountDue?: Maybe<Scalars['Float']>;
  customerName?: Maybe<Scalars['String']>;
  saveBankInfo?: Maybe<Scalars['Boolean']>;
  emailAddress?: Maybe<Scalars['String']>;
  useSavedBankInfo?: Maybe<Scalars['Boolean']>;
  savedBankInfo?: Maybe<PaymentSavedBankInfo>;
  newBankInfo?: Maybe<PaymentNewBankInfo>;
};

export type OnetimePaymentDownloadPdfRequest = {
  name?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  serviceAddress?: Maybe<Scalars['String']>;
  bankAccountRoutingNumber?: Maybe<Scalars['String']>;
  bankAccountNumber?: Maybe<Scalars['String']>;
  confirmationNumber?: Maybe<Scalars['String']>;
  paymentAmount?: Maybe<Scalars['String']>;
  paymentDate?: Maybe<Scalars['String']>;
  isFutureDatedPayment?: Maybe<Scalars['Boolean']>;
};

export type OnetimePaymentEligibilityResponse = {
   __typename?: 'OnetimePaymentEligibilityResponse';
  billMatrixUrl?: Maybe<Scalars['String']>;
  amountDue?: Maybe<Scalars['Float']>;
  isPendingDisconnect?: Maybe<Scalars['Boolean']>;
  isFutureDated?: Maybe<Scalars['Boolean']>;
  futureDatedPaymentAmount?: Maybe<Scalars['Float']>;
  isCashOnly?: Maybe<Scalars['Boolean']>;
  isNonBillableNoBalance?: Maybe<Scalars['Boolean']>;
  isSingleSameDayPayment?: Maybe<Scalars['Boolean']>;
  isMultipleSameDayPayment?: Maybe<Scalars['Boolean']>;
  lastPaidAmount?: Maybe<Scalars['Float']>;
  autoPay?: Maybe<AutoPayment>;
  oneTimePayment?: Maybe<OnetimePayment>;
};

export type OnetimePaymentPaymentInfoResponse = {
   __typename?: 'OnetimePaymentPaymentInfoResponse';
  eligibility?: Maybe<OnetimePaymentEligibilityResponse>;
  sameDayEnrolled?: Maybe<AutoPaymentStartResponse>;
  paymentPlanType?: Maybe<PaymentPlanTypeResponse>;
};

export type OnetimePaymentPdfResponse = {
   __typename?: 'OnetimePaymentPdfResponse';
  success?: Maybe<Scalars['Boolean']>;
  pdf?: Maybe<Scalars['String']>;
};

export type OnetimePaymentRequest = {
  paymentDate?: Maybe<Scalars['String']>;
  paymentAmount?: Maybe<Scalars['Float']>;
  personId: Scalars['String'];
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  accountNumber?: Maybe<Scalars['String']>;
  serviceAddressLineOne?: Maybe<Scalars['String']>;
  serviceAddressLineTwo?: Maybe<Scalars['String']>;
  amountDue?: Maybe<Scalars['Float']>;
  customerName?: Maybe<Scalars['String']>;
  saveBankInfo?: Maybe<Scalars['Boolean']>;
  emailAddress?: Maybe<Scalars['String']>;
  useSavedBankInfo?: Maybe<Scalars['Boolean']>;
  savedBankInfo?: Maybe<PaymentSavedBankInfoRequest>;
  newBankInfo?: Maybe<PaymentNewBankInfoRequest>;
};

export type OnetimePaymentResponse = {
   __typename?: 'OnetimePaymentResponse';
  success?: Maybe<Scalars['Boolean']>;
  confirmationNumber?: Maybe<Scalars['String']>;
};

export enum OnlineAccountType {
  Unknown = 'Unknown',
  PgeResidentialAcct = 'PGEResidentialAcct',
  PgeCommercialAcct = 'PGECommercialAcct',
  PgeAgencyAcct = 'PGEAgencyAcct',
  PgeEssAcct = 'PGEEssAcct'
}

export enum Operator {
  Startswith = 'STARTSWITH'
}

export type Outage = {
   __typename?: 'Outage';
  name?: Maybe<Scalars['String']>;
  coords: Coordinates;
  cause?: Maybe<Scalars['String']>;
  affected?: Maybe<Scalars['Int']>;
  calls?: Maybe<Scalars['Int']>;
  estimatedTimeOn?: Maybe<Scalars['String']>;
};

export type OutageAlert = {
   __typename?: 'OutageAlert';
  isEmail: Scalars['Boolean'];
  encryptedEmailNotificationId?: Maybe<Scalars['String']>;
  encryptedEmailContactId?: Maybe<Scalars['String']>;
  isText: Scalars['Boolean'];
  encryptedTextNotificationId?: Maybe<Scalars['String']>;
  encryptedTextContactId?: Maybe<Scalars['String']>;
};

export type OutageDetail = {
   __typename?: 'OutageDetail';
  isEligibleToReport?: Maybe<Scalars['Boolean']>;
  accountNumber?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  encryptedPremiseId?: Maybe<Scalars['String']>;
  encryptedServicePointId?: Maybe<Scalars['String']>;
  hasKnownOutage?: Maybe<Scalars['Boolean']>;
  serviceAddress?: Maybe<OutageServiceAddress>;
  registeredPhone?: Maybe<Scalars['String']>;
  callbackPhone?: Maybe<Scalars['String']>;
  callbackRequested?: Maybe<Scalars['Boolean']>;
  estimatedTimeOut?: Maybe<Scalars['String']>;
  estimatedTimeOn?: Maybe<Scalars['String']>;
  numberOfCustomersAffected?: Maybe<Scalars['Int']>;
  numberOfReports?: Maybe<Scalars['Int']>;
  crewDispatchedStatus?: Maybe<Scalars['String']>;
  isCrewOnsite?: Maybe<Scalars['Boolean']>;
  cause?: Maybe<Scalars['String']>;
  restorePowerAlert?: Maybe<OutageAlert>;
  ineligibleReason?: Maybe<Scalars['String']>;
  multipleAccountList?: Maybe<Array<Maybe<OutageMultipleAccountDetail>>>;
};

export type OutageDetailByGuestParams = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
  lookUpType: OutageLookUpType;
};

export type OutageDetailParams = {
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export enum OutageLookUpType {
  Account = 'ACCOUNT',
  Phone = 'PHONE'
}

export type OutageMultipleAccountDetail = {
   __typename?: 'OutageMultipleAccountDetail';
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  houseNumber?: Maybe<Scalars['String']>;
};

export type OutageServiceAddress = {
   __typename?: 'OutageServiceAddress';
  addressLine1?: Maybe<Scalars['String']>;
  addressLine2?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
};

export type PagingParams = {
  limit: Scalars['Int'];
  offset: Scalars['Int'];
};

export type PaymentBankInfoResponse = {
   __typename?: 'PaymentBankInfoResponse';
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  routingNumber?: Maybe<Scalars['String']>;
  bankAccountNumber?: Maybe<Scalars['String']>;
  isPaymentDueInNextThreeDays?: Maybe<Scalars['Boolean']>;
  amountDue?: Maybe<Scalars['Float']>;
  dueDate?: Maybe<Scalars['String']>;
};

export type PaymentEligibilityRequest = {
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type PaymentNewBankInfo = {
   __typename?: 'PaymentNewBankInfo';
  bankRoutingNumber?: Maybe<Scalars['String']>;
  bankAccountNumber?: Maybe<Scalars['String']>;
};

export type PaymentNewBankInfoRequest = {
  bankRoutingNumber?: Maybe<Scalars['String']>;
  bankAccountNumber?: Maybe<Scalars['String']>;
};

export type PaymentPlanTypeResponse = {
   __typename?: 'PaymentPlanTypeResponse';
  paymentPlanType?: Maybe<Scalars['String']>;
};

export type PaymentSavedBankInfo = {
   __typename?: 'PaymentSavedBankInfo';
  encryptedBankAccountNumber?: Maybe<Scalars['String']>;
  bankAccountNumber?: Maybe<Scalars['String']>;
  maskedBankAccountNumber?: Maybe<Scalars['String']>;
  bankRoutingNumber?: Maybe<Scalars['String']>;
};

export type PaymentSavedBankInfoRequest = {
  encryptedBankAccountNumber?: Maybe<Scalars['String']>;
  bankAccountNumber?: Maybe<Scalars['String']>;
  maskedBankAccountNumber?: Maybe<Scalars['String']>;
  bankRoutingNumber?: Maybe<Scalars['String']>;
};

export enum PeakTimeEnrollmentStatus {
  Enrolled = 'Enrolled',
  Unenrolled = 'Unenrolled',
  YetToEnroll = 'YetToEnroll'
}

export type PeakTimeRebateParams = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type PeakTimeRebateProgramStatus = {
   __typename?: 'PeakTimeRebateProgramStatus';
  hasActiveSA: Scalars['Boolean'];
  peakTimeRebateEnrollmentStatus: PeakTimeEnrollmentStatus;
  hasRebates: Scalars['Boolean'];
};

export type PeakTimeRebateProgramStatusResponse = {
  hasActiveSa?: Maybe<Scalars['Boolean']>;
  peakTimeRebateEnrollmentStatus?: Maybe<PeakTimeEnrollmentStatus>;
  hasRebates?: Maybe<Scalars['Boolean']>;
};

export type PendingDisconnectStatus = {
   __typename?: 'PendingDisconnectStatus';
  isPendingDisconnect: Scalars['Boolean'];
};

export type PersonAccountRequest = {
  username?: Maybe<Scalars['String']>;
  accountNumberList?: Maybe<Array<Maybe<Scalars['String']>>>;
  businessPersonAccountList?: Maybe<Array<Maybe<BusinessPersonRequest>>>;
};

export enum PersonalIdentificationType {
  None = 'NONE',
  Ssn = 'SSN',
  Dl = 'DL',
  Ortrib = 'ORTRIB',
  Visa = 'VISA',
  Military = 'MILITARY',
  Pssprt = 'PSSPRT',
  Resalien = 'RESALIEN',
  Pincode = 'PINCODE',
  Dob = 'DOB',
  Ein = 'EIN'
}

export enum PhoneType {
  None = 'None',
  Home = 'Home',
  Mobile = 'Mobile',
  Work = 'Work'
}

export type PotentialAddress = {
   __typename?: 'PotentialAddress';
  moniker?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  isFullAddress?: Maybe<Scalars['Boolean']>;
  isMultipleAddress?: Maybe<Scalars['Boolean']>;
  partialAddress?: Maybe<Scalars['String']>;
};

export type PreferredDueDateAddRequest = {
  encryptedAccountNumber: Scalars['String'];
  preferredDueDate?: Maybe<Scalars['Int']>;
};

export type PreferredDueDateAddResponse = {
   __typename?: 'PreferredDueDateAddResponse';
  preferredDueDate?: Maybe<Scalars['Int']>;
  status?: Maybe<PreferredDueDateStatus>;
  effectiveDate?: Maybe<Scalars['String']>;
};

export type PreferredDueDateEligibilityResponse = {
   __typename?: 'PreferredDueDateEligibilityResponse';
  reasonCode?: Maybe<Scalars['String']>;
  isEligible?: Maybe<Scalars['Boolean']>;
};

export type PreferredDueDateInfoResponse = {
   __typename?: 'PreferredDueDateInfoResponse';
  preferredDueDate?: Maybe<Scalars['Int']>;
  status?: Maybe<PreferredDueDateStatus>;
  effectiveDate?: Maybe<Scalars['String']>;
};

export type PreferredDueDateRequest = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type PreferredDueDateResponse = {
   __typename?: 'PreferredDueDateResponse';
  dueDateInfo?: Maybe<PreferredDueDateInfoResponse>;
  eligibility?: Maybe<PreferredDueDateEligibilityResponse>;
};

export enum PreferredDueDateStatus {
  Found = 'Found',
  NotFound = 'NotFound',
  Added = 'Added',
  NotAdded = 'NotAdded',
  ChangedToday = 'ChangedToday',
  NotChanged = 'NotChanged',
  Error = 'Error',
  NotEligible = 'NotEligible'
}

export enum PremiseType {
  None = 'None',
  Own = 'Own',
  Rent = 'Rent'
}

export type ProfilePreferencesRequest = {
  language?: Maybe<Scalars['String']>;
};

export type PromotionalOpportunity = {
   __typename?: 'PromotionalOpportunity';
  isUsingCompanyName?: Maybe<Scalars['Boolean']>;
  companyName?: Maybe<Scalars['String']>;
};

export type PromotionalOpportunityInput = {
  isUsingCompanyName?: Maybe<Scalars['Boolean']>;
  companyName?: Maybe<Scalars['String']>;
};

export type QasSearchResult = {
   __typename?: 'QasSearchResult';
  verifyLevel?: Maybe<VerifyLevel>;
  isAddressTruncated?: Maybe<Scalars['Boolean']>;
  suggestedAddress?: Maybe<SuggestedAddress>;
  potentialAddresses?: Maybe<Array<Maybe<PotentialAddress>>>;
};

export type Query = {
   __typename?: 'Query';
  root?: Maybe<Scalars['String']>;
  getAccountGroups?: Maybe<Array<Maybe<Group>>>;
  getCustomGroups?: Maybe<Array<Maybe<Group>>>;
  getAccountInfo?: Maybe<AccountCustomer>;
  getAccountDetails?: Maybe<Array<Maybe<AccountDetail>>>;
  getAccountDetailList?: Maybe<AccountDetailList>;
  getAlertDetails?: Maybe<AlertDetails>;
  getBillingAndPaymentHistoryDetails?: Maybe<BillingAndPaymentHistoryDetailsResponse>;
  downloadBillingAndPaymentHistoryDetails?: Maybe<BillingAndPaymentHistoryDetailsDownloadResponse>;
  getBillInfo?: Maybe<BillInfo>;
  isPaperlessBillEnrolled?: Maybe<IsPaperlessBillEnrolledResponse>;
  getViewBillInfo?: Maybe<ViewBillInfoResponse>;
  viewBillDownloadPdf?: Maybe<ViewBillDownloadPdfResponse>;
  getViewPaymentHistoryChargeSummary?: Maybe<ViewPaymentHistoryResponse>;
  getViewPaymentHistoryDetail?: Maybe<ViewPaymentHistoryDetailResponse>;
  getViewPaymentHistoryUsage?: Maybe<ViewPaymentHistoryBillingUsageResponse>;
  getViewPaymentHistoryDetailFromCloud?: Maybe<ViewPaymentHistoryResponse>;
  validateAccountExists?: Maybe<ValidateAccountExistsResponse>;
  isPersonalIdentificationValid?: Maybe<Scalars['Boolean']>;
  isCustomerServiceDisconnected?: Maybe<Scalars['Boolean']>;
  getCoCustomerPersonDetails?: Maybe<AdditionalInfo>;
  getMainCustomerPersonDetails?: Maybe<AdditionalInfo>;
  getCoCustomersForAccount?: Maybe<Array<Maybe<CoCustomerInfo>>>;
  isDuplicateEmail?: Maybe<IsDuplicateEmailResponse>;
  getMailingAddressForAccount?: Maybe<Address>;
  getPnpPhone?: Maybe<Scalars['String']>;
  getPersonsForAccount?: Maybe<Array<Maybe<AccountPerson>>>;
  getPersonPrimaryIdentificationType?: Maybe<PersonalIdentificationType>;
  qasSearchAddress?: Maybe<QasSearchResult>;
  qasSearchAddressById?: Maybe<QasSearchResult>;
  qasRefineAddressSearch?: Maybe<QasSearchResult>;
  searchServiceAddress?: Maybe<SearchServiceAddressResponse>;
  moveServiceEligibility?: Maybe<MoveServiceEligibilityResponse>;
  moveToServiceAddressEligibility?: Maybe<MoveToServiceAddressEligibilityResponse>;
  getOutageDetail?: Maybe<OutageDetail>;
  getOutageDetailByGuest?: Maybe<OutageDetail>;
  getOutages?: Maybe<Array<Maybe<Outage>>>;
  getAutoPaymentInfo?: Maybe<AutoPaymentInfoResponse>;
  getDownloadAutoPaymentPdf?: Maybe<AutoPaymentPdfResponse>;
  getGuestPaymentInfo?: Maybe<GuestPaymentDetailsResponse>;
  getDownloadGuestPaymentPdf?: Maybe<GuestPaymentPdfResponse>;
  getGroupAmountDetails?: Maybe<OnecheckPaymentInfoResponse>;
  getDownloadOnecheckRemittanceFormPdf?: Maybe<OnecheckPaymentDownloadRemittanceFormResponse>;
  getOnetimePaymentInfo?: Maybe<OnetimePaymentPaymentInfoResponse>;
  getDownloadOnetimePaymentPdf?: Maybe<OnetimePaymentPdfResponse>;
  verifyRoutingNumber?: Maybe<VerifyRoutingResponse>;
  getPreferredDueDateInfo?: Maybe<PreferredDueDateResponse>;
  getQuickPaymentDetails?: Maybe<QuickPaymentInfoResponse>;
  getPeakTimeRebateStatus?: Maybe<PeakTimeRebateProgramStatus>;
  renewablesAccountEligibility?: Maybe<RenewablesAccountEligibleResponse>;
  getRenewables?: Maybe<RenewablePower>;
  getRenewableEnrollmentStatus?: Maybe<RenewableEnrollment>;
  validateCommercialAccountNumber?: Maybe<CommercialAccountValidation>;
  startServiceAddressEligibility?: Maybe<ServiceAddressEligibility>;
  startServiceEligibility?: Maybe<StartServiceEligibilityResponse>;
  stopServiceEligibility?: Maybe<Scalars['Boolean']>;
  hasPerson?: Maybe<HasPersonIdResponse>;
  sameAsOldPassword?: Maybe<SameAsOldPasswordResponse>;
};


export type QueryGetAccountGroupsArgs = {
  params?: Maybe<AccountGroupParams>;
};


export type QueryGetAccountDetailsArgs = {
  params: AccountDetailParams;
};


export type QueryGetAccountDetailListArgs = {
  params: AccountDetailListParams;
};


export type QueryGetAlertDetailsArgs = {
  params?: Maybe<GetAlertDetailsRequest>;
};


export type QueryGetBillingAndPaymentHistoryDetailsArgs = {
  payload?: Maybe<BillingAndPaymentHistoryDetailsInput>;
};


export type QueryDownloadBillingAndPaymentHistoryDetailsArgs = {
  payload?: Maybe<BillingAndPaymentHistoryDetailsInput>;
};


export type QueryGetBillInfoArgs = {
  params?: Maybe<BillInfoParams>;
};


export type QueryIsPaperlessBillEnrolledArgs = {
  payload?: Maybe<IsPaperlessBillEnrolledRequest>;
};


export type QueryGetViewBillInfoArgs = {
  payload: ViewBillDetailsRequest;
};


export type QueryViewBillDownloadPdfArgs = {
  payload?: Maybe<ViewBillDownloadPdfRequest>;
};


export type QueryGetViewPaymentHistoryChargeSummaryArgs = {
  payload?: Maybe<ViewPaymentHistoryInput>;
};


export type QueryGetViewPaymentHistoryDetailArgs = {
  payload?: Maybe<ViewPaymentHistoryDetailInput>;
};


export type QueryGetViewPaymentHistoryUsageArgs = {
  payload?: Maybe<ViewPaymentHistoryBillingUsageInput>;
};


export type QueryGetViewPaymentHistoryDetailFromCloudArgs = {
  payload?: Maybe<ViewPaymentHistoryInput>;
};


export type QueryValidateAccountExistsArgs = {
  payload?: Maybe<ValidateAccountExistsRequest>;
};


export type QueryIsPersonalIdentificationValidArgs = {
  params?: Maybe<IsPersonalIdValidParams>;
};


export type QueryIsCustomerServiceDisconnectedArgs = {
  payload?: Maybe<CustomerServiceDisconnectedParams>;
};


export type QueryGetCoCustomerPersonDetailsArgs = {
  encryptedPersonId?: Maybe<Scalars['String']>;
};


export type QueryGetMainCustomerPersonDetailsArgs = {
  encryptedPersonId?: Maybe<Scalars['String']>;
};


export type QueryGetCoCustomersForAccountArgs = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};


export type QueryIsDuplicateEmailArgs = {
  email?: Maybe<Scalars['String']>;
};


export type QueryGetMailingAddressForAccountArgs = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};


export type QueryGetPnpPhoneArgs = {
  encryptedPersonId?: Maybe<Scalars['String']>;
};


export type QueryGetPersonsForAccountArgs = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};


export type QueryQasSearchAddressArgs = {
  params?: Maybe<QuickAddressSearchParams>;
};


export type QueryQasSearchAddressByIdArgs = {
  params?: Maybe<QuickAddressSearchParams>;
};


export type QueryQasRefineAddressSearchArgs = {
  params?: Maybe<QuickAddressSearchParams>;
};


export type QuerySearchServiceAddressArgs = {
  payload?: Maybe<SearchServiceAddressRequest>;
};


export type QueryMoveServiceEligibilityArgs = {
  payload?: Maybe<MoveServiceEligibilityRequest>;
};


export type QueryMoveToServiceAddressEligibilityArgs = {
  payload?: Maybe<MoveToServiceAddressEligibilityRequest>;
};


export type QueryGetOutageDetailArgs = {
  params?: Maybe<OutageDetailParams>;
};


export type QueryGetOutageDetailByGuestArgs = {
  payload?: Maybe<OutageDetailByGuestParams>;
};


export type QueryGetAutoPaymentInfoArgs = {
  payload?: Maybe<PaymentEligibilityRequest>;
};


export type QueryGetDownloadAutoPaymentPdfArgs = {
  payload?: Maybe<AutoPaymentDownloadPdfRequest>;
};


export type QueryGetGuestPaymentInfoArgs = {
  payload?: Maybe<GuestPaymentAccountRequest>;
};


export type QueryGetDownloadGuestPaymentPdfArgs = {
  payload?: Maybe<GuestPaymentDownloadPdfRequest>;
};


export type QueryGetGroupAmountDetailsArgs = {
  payload?: Maybe<OnecheckPaymentGroupInfoRequest>;
};


export type QueryGetDownloadOnecheckRemittanceFormPdfArgs = {
  payload?: Maybe<OnecheckPaymentSubmitRequest>;
};


export type QueryGetOnetimePaymentInfoArgs = {
  payload?: Maybe<PaymentEligibilityRequest>;
};


export type QueryGetDownloadOnetimePaymentPdfArgs = {
  payload?: Maybe<OnetimePaymentDownloadPdfRequest>;
};


export type QueryVerifyRoutingNumberArgs = {
  payload?: Maybe<VerifyRoutingRequest>;
};


export type QueryGetPreferredDueDateInfoArgs = {
  payload?: Maybe<PreferredDueDateRequest>;
};


export type QueryGetQuickPaymentDetailsArgs = {
  encryptedAccountInfo?: Maybe<Scalars['String']>;
};


export type QueryGetPeakTimeRebateStatusArgs = {
  params: PeakTimeRebateParams;
};


export type QueryRenewablesAccountEligibilityArgs = {
  payload?: Maybe<RenewablesAccountEligibleRequest>;
};


export type QueryGetRenewablesArgs = {
  encryptedServiceAgreementId?: Maybe<Scalars['String']>;
};


export type QueryGetRenewableEnrollmentStatusArgs = {
  params?: Maybe<RenewableEnrollmentStatusParams>;
};


export type QueryValidateCommercialAccountNumberArgs = {
  accountNumber: Scalars['String'];
};


export type QueryStartServiceAddressEligibilityArgs = {
  payload?: Maybe<StartServiceAddressEligibilityRequest>;
};


export type QueryStartServiceEligibilityArgs = {
  payload?: Maybe<StartServiceEligibilityRequest>;
};


export type QueryStopServiceEligibilityArgs = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};


export type QuerySameAsOldPasswordArgs = {
  payload?: Maybe<SameAsOldPasswordRequest>;
};

export type QuickAddressSearchParams = {
  state?: Maybe<Scalars['String']>;
  addressLine1?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
  fullAddress?: Maybe<Scalars['String']>;
  isMailingAddress?: Maybe<Scalars['Boolean']>;
  moniker?: Maybe<Scalars['String']>;
};

export type QuickPaymentInfoResponse = {
   __typename?: 'QuickPaymentInfoResponse';
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  hasNoAutoPay?: Maybe<Scalars['Boolean']>;
  hasNoCheckStatus?: Maybe<Scalars['Boolean']>;
  amountDue?: Maybe<Scalars['Float']>;
  dueDate?: Maybe<Scalars['String']>;
  paymentDate?: Maybe<Scalars['String']>;
  serviceAddress?: Maybe<Scalars['String']>;
  encryptedPersonId?: Maybe<Scalars['String']>;
  emailId?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  savedBankInfo?: Maybe<PaymentSavedBankInfo>;
};

export type QuickPaymentRequest = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  hasNoAutoPay?: Maybe<Scalars['Boolean']>;
  hasNoCheckStatus?: Maybe<Scalars['Boolean']>;
  amountDue?: Maybe<Scalars['Float']>;
  dueDate?: Maybe<Scalars['String']>;
  paymentDate?: Maybe<Scalars['String']>;
  serviceAddress?: Maybe<Scalars['String']>;
  encryptedPersonId: Scalars['String'];
  emailId?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  savedBankInfo?: Maybe<PaymentSavedBankInfoRequest>;
};

export type QuickPaymentResponse = {
   __typename?: 'QuickPaymentResponse';
  referenceId?: Maybe<Scalars['String']>;
  success?: Maybe<Scalars['Boolean']>;
};

export type RegistrationResponse = {
   __typename?: 'RegistrationResponse';
  success?: Maybe<Scalars['Boolean']>;
  signinToken?: Maybe<Scalars['String']>;
  uid?: Maybe<Scalars['String']>;
};

export enum RelationshipType {
  Main = 'Main',
  CoApplicant = 'CoApplicant',
  Responsible = 'Responsible'
}

export enum RenewableEligibility {
  Unknown = 'Unknown',
  Eligible = 'Eligible',
  IneligibleNoActiveServiceAgreement = 'IneligibleNoActiveServiceAgreement',
  IneligibleHasDisconnect = 'IneligibleHasDisconnect',
  IneligibleHasMultipleServiceAgreement = 'IneligibleHasMultipleServiceAgreement'
}

export type RenewableEnrollment = {
   __typename?: 'RenewableEnrollment';
  renewableEnrollmentStatus?: Maybe<RenewableEnrollmentStatus>;
};

export enum RenewableEnrollmentStatus {
  Unknown = 'Unknown',
  Enrolled = 'Enrolled',
  NotEnrolled = 'NotEnrolled',
  Canceled = 'Canceled',
  Changed = 'Changed'
}

export type RenewableEnrollmentStatusParams = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type RenewableOption = {
   __typename?: 'RenewableOption';
  greenSourceEnrolled?: Maybe<Scalars['Boolean']>;
  cleanWindEnrolled?: Maybe<Scalars['Boolean']>;
  greenFutureSolarEnrolled?: Maybe<Scalars['Boolean']>;
  supportingHabitatEnrolled?: Maybe<Scalars['Boolean']>;
  cleanWindChargeBlock?: Maybe<Scalars['Int']>;
  greenFutureSolarBlock?: Maybe<Scalars['Int']>;
  cleanWindStartDate?: Maybe<Scalars['String']>;
  cleanWindEndDate?: Maybe<Scalars['String']>;
  supportHabitatStartDate?: Maybe<Scalars['String']>;
  supportHabitatEndDate?: Maybe<Scalars['String']>;
  greenSourceStartDate?: Maybe<Scalars['String']>;
  greenSourceEndDate?: Maybe<Scalars['String']>;
  cleanWindQuantityEffectiveDate?: Maybe<Scalars['String']>;
};

export type RenewableOptionInput = {
  greenSourceEnrolled?: Maybe<Scalars['Boolean']>;
  cleanWindEnrolled?: Maybe<Scalars['Boolean']>;
  greenFutureSolarEnrolled?: Maybe<Scalars['Boolean']>;
  supportingHabitatEnrolled?: Maybe<Scalars['Boolean']>;
  cleanWindChargeBlock?: Maybe<Scalars['Int']>;
  greenFutureSolarBlock?: Maybe<Scalars['Int']>;
  totalGreenFutureSolarBlock?: Maybe<Scalars['Float']>;
  totalCleanWindChargeBlock?: Maybe<Scalars['Float']>;
  cleanWindStartDate?: Maybe<Scalars['String']>;
  cleanWindEndDate?: Maybe<Scalars['String']>;
  supportHabitatStartDate?: Maybe<Scalars['String']>;
  supportHabitatEndDate?: Maybe<Scalars['String']>;
  greenSourceStartDate?: Maybe<Scalars['String']>;
  greenSourceEndDate?: Maybe<Scalars['String']>;
  cleanWindQuantityEffectiveDate?: Maybe<Scalars['String']>;
};

export type RenewablePower = {
   __typename?: 'RenewablePower';
  renewableOption?: Maybe<RenewableOption>;
  eligibility?: Maybe<RenewableEligibility>;
  isSmallBusiness?: Maybe<Scalars['Boolean']>;
  isIndustrial?: Maybe<Scalars['Boolean']>;
  howDidYouHearOption?: Maybe<Scalars['String']>;
  keyWord?: Maybe<Scalars['String']>;
  promotionalOpportunity?: Maybe<PromotionalOpportunity>;
  originalRenewableOption?: Maybe<RenewableOption>;
  username?: Maybe<Scalars['String']>;
  contactName?: Maybe<Scalars['String']>;
  primaryPhoneNumber?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  encryptedServiceAgreementId?: Maybe<Scalars['String']>;
};

export type RenewablePowerInput = {
  username?: Maybe<Scalars['String']>;
  renewableOption?: Maybe<RenewableOptionInput>;
  originalRenewableOption?: Maybe<RenewableOptionInput>;
  howDidYouHearOption?: Maybe<Scalars['String']>;
  keyWord?: Maybe<Scalars['String']>;
  promotionalOpportunity?: Maybe<PromotionalOpportunityInput>;
  isSmallBusiness?: Maybe<Scalars['Boolean']>;
  isIndustrial?: Maybe<Scalars['Boolean']>;
  eligibility?: Maybe<RenewableEligibility>;
  encryptedServiceAgreementId?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  contactName?: Maybe<Scalars['String']>;
  primaryPhoneNumber?: Maybe<Scalars['String']>;
};

export type RenewablesAccountEligibleRequest = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  addressLine1: Scalars['String'];
  city: Scalars['String'];
  state: Scalars['String'];
  postal: Scalars['String'];
};

export type RenewablesAccountEligibleResponse = {
   __typename?: 'RenewablesAccountEligibleResponse';
  eligibility?: Maybe<RenewableEligibility>;
  isIndustrial?: Maybe<Scalars['Boolean']>;
  isSmallBusiness?: Maybe<Scalars['Boolean']>;
  encryptedServiceAgreementId?: Maybe<Scalars['String']>;
};

export type RenewablesEnrollmentToDoRequest = {
  contactName?: Maybe<Scalars['String']>;
  primaryPhoneNumber?: Maybe<Scalars['String']>;
  username?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  encryptedServiceAgreementId?: Maybe<Scalars['String']>;
};

export type ReportOutageByGuestRequest = {
  encryptedAccountNumber: Scalars['String'];
  encryptedPremiseId: Scalars['String'];
  registeredPhone?: Maybe<Scalars['String']>;
  callbackPhone?: Maybe<Scalars['String']>;
  callbackRequested: Scalars['Boolean'];
  previouslyEnrolledInCallbackPhone: Scalars['Boolean'];
};

export type ReportOutageRequest = {
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
  encryptedPremiseId: Scalars['String'];
  registeredPhone?: Maybe<Scalars['String']>;
  callbackPhone?: Maybe<Scalars['String']>;
  callbackRequested: Scalars['Boolean'];
  restorePowerAlert?: Maybe<ReportOutageRequest_Alert>;
  previouslyEnrolledInCallbackPhone: Scalars['Boolean'];
  previouslyEnrolledInAlerts: Scalars['Boolean'];
};

export type ReportOutageRequest_Alert = {
  isEmail: Scalars['Boolean'];
  encryptedEmailNotificationId?: Maybe<Scalars['String']>;
  encryptedEmailContactId?: Maybe<Scalars['String']>;
  isText: Scalars['Boolean'];
  encryptedTextNotificationId?: Maybe<Scalars['String']>;
  encryptedTextContactId?: Maybe<Scalars['String']>;
};

export type ReportOutageResponse = {
   __typename?: 'ReportOutageResponse';
  success?: Maybe<Scalars['Boolean']>;
};

export type ResidentialRegistrationRequest = {
  AccountNumber?: Maybe<Scalars['String']>;
  EmailAddress: Scalars['String'];
  Password: Scalars['String'];
  PhoneNumber?: Maybe<Scalars['String']>;
  Last4DigitsOfSSN?: Maybe<Scalars['String']>;
  Last4DigitsOfDriversLicenseOrStateId?: Maybe<Scalars['String']>;
  PinCode?: Maybe<Scalars['String']>;
  IsPaperlessBillingSelected?: Maybe<Scalars['Boolean']>;
};

export type ResponseBase = {
  success?: Maybe<Scalars['Boolean']>;
  code?: Maybe<Scalars['Int']>;
  message?: Maybe<Scalars['String']>;
};

export type SameAsOldPasswordRequest = {
  password?: Maybe<Scalars['String']>;
};

export type SameAsOldPasswordResponse = {
   __typename?: 'SameAsOldPasswordResponse';
  sameAsOldPassword?: Maybe<Scalars['Boolean']>;
};

export enum SartServicePremiseTypeParam {
  None = 'None',
  Own = 'Own',
  Rent = 'Rent'
}

export type SearchServiceAddress = {
   __typename?: 'SearchServiceAddress';
  addressLine1: Scalars['String'];
  city: Scalars['String'];
  postal: Scalars['String'];
};

export type SearchServiceAddressRequest = {
  match?: Maybe<Scalars['String']>;
};

export type SearchServiceAddressResponse = {
   __typename?: 'SearchServiceAddressResponse';
  addresses?: Maybe<Array<Maybe<SearchServiceAddress>>>;
};

export type SendActivationCodeRequest = {
  phoneNumber: Scalars['String'];
};

export type SendActivationCodeResponse = {
   __typename?: 'SendActivationCodeResponse';
  encryptedActivationCode: Scalars['String'];
};

export type ServiceAddress = {
   __typename?: 'ServiceAddress';
  addressLine1?: Maybe<Scalars['String']>;
  addressLine2?: Maybe<Scalars['String']>;
  houseType?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  county?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  pMBMailstop?: Maybe<Scalars['String']>;
  fullServiceAddress?: Maybe<Scalars['String']>;
  fullStreetAddress1?: Maybe<Scalars['String']>;
  fullStreetAddress2?: Maybe<Scalars['String']>;
  inCareOf?: Maybe<Scalars['String']>;
  qasVerified?: Maybe<Scalars['Boolean']>;
};

export type ServiceAddressEligibility = {
   __typename?: 'ServiceAddressEligibility';
  isEligible?: Maybe<Scalars['Boolean']>;
  isAddressExactMatch?: Maybe<Scalars['Boolean']>;
  isPeakTimeRebateEligible?: Maybe<Scalars['Boolean']>;
  premiseId?: Maybe<Scalars['String']>;
  serviceAddressEligibilityType?: Maybe<StartServiceAddressEligibilityType>;
};

export enum ServiceAddressEligibilityType {
  None = 'None',
  AddressNotFound = 'AddressNotFound',
  CommercialBuilding = 'CommercialBuilding'
}

export type ServiceAddressResponse = {
  addressLine1?: Maybe<Scalars['String']>;
  addressLine2?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  county?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
};

export type SortParams = {
  sort: AccountSort;
  direction: Direction;
};

export type StartServiceAddressEligibilityRequest = {
  accountNumber?: Maybe<Scalars['String']>;
  serviceAddress: StartServiceAddressParam;
  selectedPremiseType: SartServicePremiseTypeParam;
};

export enum StartServiceAddressEligibilityType {
  None = 'None',
  AddressNotFound = 'AddressNotFound',
  CommercialBuilding = 'CommercialBuilding'
}

export type StartServiceAddressParam = {
  addressLine1: Scalars['String'];
  city: Scalars['String'];
  state: Scalars['String'];
  postal: Scalars['String'];
};

export type StartServiceEligibilityRequest = {
  accountNumber: Scalars['String'];
};

export type StartServiceEligibilityResponse = {
   __typename?: 'StartServiceEligibilityResponse';
  isEligible?: Maybe<Scalars['Boolean']>;
};

export type StartServiceIneligibilityLogRequest = {
  accountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type StartServiceRequest = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  channel?: Maybe<Scalars['String']>;
  coCustomerInformation?: Maybe<CustomerInfoInput>;
  currentServiceAddress?: Maybe<AddressInput>;
  customerInformation?: Maybe<CustomerInfoInput>;
  eligibleForGreenResource?: Maybe<Scalars['Boolean']>;
  heatSourceType?: Maybe<Scalars['String']>;
  isCoCustomerSelected?: Maybe<Scalars['Boolean']>;
  isCurrentlyPaperlessBilling?: Maybe<Scalars['Boolean']>;
  isElectricVehicleSelected?: Maybe<Scalars['Boolean']>;
  isPaperlessBillingSelected?: Maybe<Scalars['Boolean']>;
  peakTimeRebateEmail?: Maybe<Scalars['String']>;
  peakTimeRebateMobilePhone?: Maybe<Scalars['String']>;
  isRenewablesSelected?: Maybe<Scalars['Boolean']>;
  isSmartThermostatSelected?: Maybe<Scalars['Boolean']>;
  isUserLoggedIn?: Maybe<Scalars['Boolean']>;
  livesAtPremise?: Maybe<Scalars['Boolean']>;
  meterAccessInfo?: Maybe<MeterAccessInput>;
  selectedPremiseType?: Maybe<PremiseType>;
  serviceAddress?: Maybe<AddressInput>;
  startDate?: Maybe<Scalars['String']>;
  submitDate?: Maybe<Scalars['String']>;
  waterHeaterType?: Maybe<Scalars['String']>;
  preferredDueDateSelected?: Maybe<Scalars['Int']>;
  isAddressExactMatch?: Maybe<Scalars['Boolean']>;
  premiseId?: Maybe<Scalars['String']>;
};

export type StartServiceResponse = {
   __typename?: 'StartServiceResponse';
  isStartServiceSuccessful?: Maybe<Scalars['Boolean']>;
  isPreferredDueDateSuccessful?: Maybe<Scalars['Boolean']>;
};

export type State = {
   __typename?: 'State';
  name?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
};

export enum StateIdType {
  DriverLicense = 'DriverLicense',
  StateIdCard = 'StateIDCard'
}

export type StateInfo = {
   __typename?: 'StateInfo';
  stateIdentificationType?: Maybe<StateIdType>;
  stateIDState?: Maybe<State>;
  stateIDNumber?: Maybe<Scalars['String']>;
};

export type StateInfoInput = {
  stateIdentificationType?: Maybe<StateIdType>;
  stateIDState?: Maybe<StateInput>;
  stateIDNumber?: Maybe<Scalars['String']>;
};

export type StateInput = {
  name?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
};

export type StopServiceAddressInput = {
  addressLine1?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  qasVerified?: Maybe<Scalars['Boolean']>;
  inCareOf?: Maybe<Scalars['String']>;
};

export type StopServiceSubmitRequest = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  phoneNumber?: Maybe<Scalars['String']>;
  serviceAddress?: Maybe<AddressInput>;
  anyoneRemainingAtProperty?: Maybe<Scalars['Boolean']>;
  stopDate?: Maybe<Scalars['String']>;
  finalBillAddress?: Maybe<StopServiceAddressInput>;
  phoneExt?: Maybe<Scalars['String']>;
};

export type SubmitUpdateInfoInput = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  originalUpdateAccountInfo?: Maybe<UpdateAccountInfoInput>;
  updateInfo?: Maybe<UpdateAccountInfoInput>;
  serviceAddress?: Maybe<AddressInput>;
  accountType?: Maybe<OnlineAccountType>;
};

export type SuggestedAddress = {
   __typename?: 'SuggestedAddress';
  addressLine1?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  isGeneralAddress?: Maybe<Scalars['Boolean']>;
  isHighwayContractRoute?: Maybe<Scalars['Boolean']>;
};

export type UnregisterResponse = {
   __typename?: 'UnregisterResponse';
  success?: Maybe<Scalars['Boolean']>;
};

export type UpdateAccountInfoInput = {
  primaryPhone?: Maybe<Scalars['String']>;
  accountType?: Maybe<OnlineAccountType>;
  altPhoneInfo?: Maybe<AlternatePhoneInfoInput>;
  commContactInfo?: Maybe<CommercialContactInfoInput>;
  mailingAddress?: Maybe<AddressInput>;
  meterAccessInfo?: Maybe<MeterAccessInput>;
  persons?: Maybe<Array<Maybe<AccountPersonInput>>>;
};

export enum UpdateAlertsError {
  None = 'None',
  PhoneAttachedToActiveAccount = 'PhoneAttachedToActiveAccount',
  Other = 'Other'
}

export type UpdateAlertsRequest = {
  encryptedPersonId?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  newPhoneNumber?: Maybe<Scalars['String']>;
  originalMobilePhoneNumber?: Maybe<Scalars['String']>;
  originalMobilePhoneSequence?: Maybe<Scalars['Float']>;
  originallyNotEnrolled?: Maybe<Scalars['Boolean']>;
  alerts?: Maybe<Array<Maybe<Alertinput>>>;
};

export type UpdateAlertsResponse = {
   __typename?: 'UpdateAlertsResponse';
  updateAlertsError?: Maybe<UpdateAlertsError>;
};

export type UpdateGroupInput = {
  groupId: Scalars['String'];
  groupName?: Maybe<Scalars['String']>;
  groupCode?: Maybe<Scalars['String']>;
  isDefault?: Maybe<Scalars['Boolean']>;
};

export type UpdateGroupResponse = ResponseBase & {
   __typename?: 'UpdateGroupResponse';
  success?: Maybe<Scalars['Boolean']>;
  code?: Maybe<Scalars['Int']>;
  message?: Maybe<Scalars['String']>;
};

export type UpdatePaperlessBillRequest = {
  encryptedAccountNumber: Scalars['String'];
  isPaperless: Scalars['Boolean'];
  email: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type UpdatePaperlessBillResponse = {
   __typename?: 'UpdatePaperlessBillResponse';
  success: Scalars['Boolean'];
};

export type ValidateAccountExistsRequest = {
  value: Scalars['String'];
  lookUpType: AccountLookUpType;
};

export type ValidateAccountExistsResponse = {
   __typename?: 'ValidateAccountExistsResponse';
  result: Scalars['Boolean'];
};

export enum VerificationType {
  Ein = 'EIN',
  Phone = 'PHONE',
  Dob = 'DOB'
}

export type VerifyAccountGroupDetailsReponse = {
   __typename?: 'VerifyAccountGroupDetailsReponse';
  reason?: Maybe<VerifyAccountGroupReason>;
  personId?: Maybe<Scalars['String']>;
};

export type VerifyAccountGroupDetailsRequest = {
  accountNumber: Scalars['String'];
  businessName: Scalars['String'];
  verificationValue: Scalars['String'];
  verificationType: AccountGroupVerificationType;
};

export enum VerifyAccountGroupReason {
  VerificationFailed = 'VerificationFailed',
  PersonFound = 'PersonFound',
  PersonNotFound = 'PersonNotFound',
  AccountNotFound = 'AccountNotFound'
}

export type VerifyEqualsEncryptedValueRequest = {
  clearTextValue: Scalars['String'];
  encryptedValue: Scalars['String'];
};

export type VerifyEqualsEncryptedValueResponse = {
   __typename?: 'VerifyEqualsEncryptedValueResponse';
  result: Scalars['Boolean'];
};

export enum VerifyLevel {
  None = 'None',
  Verified = 'Verified',
  InteractionRequired = 'InteractionRequired',
  Multiple = 'Multiple',
  PremisesPartial = 'PremisesPartial',
  StreetPartial = 'StreetPartial'
}

export type VerifyRoutingRequest = {
  routingNumber?: Maybe<Scalars['String']>;
};

export type VerifyRoutingResponse = {
   __typename?: 'VerifyRoutingResponse';
  valid?: Maybe<Scalars['Boolean']>;
};

export type ViewBillAverageTemperature = {
   __typename?: 'ViewBillAverageTemperature';
  temperatureSource?: Maybe<Scalars['String']>;
  currentBillingPeriod?: Maybe<ViewBillAverageTemperatureBillingPeriod>;
  previousBillingPeriod?: Maybe<ViewBillAverageTemperatureBillingPeriod>;
};

export type ViewBillAverageTemperatureBillingPeriod = {
   __typename?: 'ViewBillAverageTemperatureBillingPeriod';
  date?: Maybe<Scalars['DateTime']>;
  averageTemperature?: Maybe<Scalars['Float']>;
  totalCost?: Maybe<Scalars['Float']>;
  totalKwh?: Maybe<Scalars['Float']>;
};

export type ViewBillDetails = {
   __typename?: 'ViewBillDetails';
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  downloadBillUrl?: Maybe<Scalars['String']>;
  billStatus?: Maybe<Scalars['String']>;
  billDate?: Maybe<Scalars['DateTime']>;
  dueDate?: Maybe<Scalars['DateTime']>;
  amountDue?: Maybe<Scalars['Float']>;
  previousBalance?: Maybe<Scalars['Float']>;
  totalAdjustments?: Maybe<Scalars['Float']>;
  totalCurrentCharges?: Maybe<Scalars['Float']>;
  totalBalanceAfterBill?: Maybe<Scalars['Float']>;
  hasBills?: Maybe<Scalars['Boolean']>;
};

export type ViewBillDetailsRequest = {
  encryptedAccountNumber: Scalars['String'];
};

export type ViewBillDownloadPdfRequest = {
  encryptedBillId?: Maybe<Scalars['String']>;
};

export type ViewBillDownloadPdfResponse = {
   __typename?: 'ViewBillDownloadPdfResponse';
  pdf?: Maybe<Scalars['String']>;
  success?: Maybe<Scalars['Boolean']>;
};

export type ViewBillInfoResponse = {
   __typename?: 'ViewBillInfoResponse';
  viewBillDetails?: Maybe<ViewBillDetails>;
  viewBillAverageTemperature?: Maybe<ViewBillAverageTemperature>;
  viewBillMonthUsage?: Maybe<ViewBillMonthlyUsage>;
};

export type ViewBillMonthlyUsage = {
   __typename?: 'ViewBillMonthlyUsage';
  usages?: Maybe<Array<Maybe<ViewBillMonthlyUsageDetails>>>;
};

export type ViewBillMonthlyUsageDetails = {
   __typename?: 'ViewBillMonthlyUsageDetails';
  monthName?: Maybe<Scalars['String']>;
  year?: Maybe<Scalars['String']>;
  totalKwh?: Maybe<Scalars['Float']>;
};

export enum ViewPaymentHistoryBillingAndPaymentType {
  Unknown = 'Unknown',
  Bill = 'Bill',
  Payment = 'Payment',
  PendingPayment = 'PendingPayment',
  Agency = 'Agency'
}

export type ViewPaymentHistoryBillingUsageInput = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type ViewPaymentHistoryBillingUsageResponse = {
   __typename?: 'ViewPaymentHistoryBillingUsageResponse';
  billingUsages?: Maybe<Array<Maybe<ViewPaymentHistoryDetail>>>;
};

export type ViewPaymentHistoryDetail = {
   __typename?: 'ViewPaymentHistoryDetail';
  date?: Maybe<Scalars['DateTime']>;
  billingPeriodStartDate?: Maybe<Scalars['DateTime']>;
  billingPeriodEndDate?: Maybe<Scalars['DateTime']>;
  amountDue?: Maybe<Scalars['Float']>;
  kwh?: Maybe<Scalars['Float']>;
  amountPaid?: Maybe<Scalars['Float']>;
  encryptedBillId?: Maybe<Scalars['String']>;
  type?: Maybe<ViewPaymentHistoryBillingAndPaymentType>;
};

export type ViewPaymentHistoryDetailInput = {
  pageIndex?: Maybe<Scalars['Int']>;
  noOfDisplay?: Maybe<Scalars['Int']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type ViewPaymentHistoryDetailResponse = {
   __typename?: 'ViewPaymentHistoryDetailResponse';
  paymentHistoryDetails?: Maybe<Array<Maybe<ViewPaymentHistoryDetail>>>;
};

export type ViewPaymentHistoryInput = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['DateTime']>;
  endDate?: Maybe<Scalars['DateTime']>;
};

export type ViewPaymentHistoryResponse = {
   __typename?: 'ViewPaymentHistoryResponse';
  amountDue?: Maybe<Scalars['Float']>;
  paymentAdjustments?: Maybe<Scalars['Float']>;
  balanceForward?: Maybe<Scalars['Float']>;
  dueDate?: Maybe<Scalars['DateTime']>;
  billingAndPaymentDetails?: Maybe<Array<Maybe<ViewPaymentHistoryDetail>>>;
  totalDetailsRecords?: Maybe<Scalars['Float']>;
};
